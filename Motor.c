/*
 * Motor.c
 *
 *  Created on: May 16, 2018
 *      Author: Ryu
 */
#include "CommonHeader.h"

void set_Sense(uint8_t sense_Status)
{
	if (sense_Status == (uint8_t)right)
	{
		DIGITAL_IO_SetOutputHigh (&IN1_front);
		DIGITAL_IO_SetOutputLow(&IN2_front);
	}
	else if(sense_Status == (uint8_t)left)
	{
		DIGITAL_IO_SetOutputHigh (&IN2_front);
	    DIGITAL_IO_SetOutputLow(&IN1_front);
	}
	else if(sense_Status == (uint8_t)straight)
	{
		DIGITAL_IO_SetOutputHigh (&IN2_front);
	    DIGITAL_IO_SetOutputHigh(&IN1_front);
	}
}

void set_Direction(uint8_t direction_Status)
{
	if (direction_Status == (uint8_t)forward)
	{
		DIGITAL_IO_SetOutputHigh (&IN4_back);
		DIGITAL_IO_SetOutputLow(&IN3_back);
	}
	else if(direction_Status == (uint8_t)backward)
	{
		DIGITAL_IO_SetOutputHigh (&IN3_back);
	    DIGITAL_IO_SetOutputLow(&IN4_back);
	}

}

void set_Motors(command decoded_message)
{
	set_Direction(decoded_message.direction);
	set_Sense(decoded_message.sense);
	PWM_SetDutyCycle(&motor_back,10000/127*((int)decoded_message.motor_value[back]));

}

// USE THIS WITH CARE FOR PASENGERS
void push_Breakes(void)
{
	   DIGITAL_IO_SetOutputLow(&IN2_front);
	   DIGITAL_IO_SetOutputLow(&IN1_front);
	   DIGITAL_IO_SetOutputLow(&IN3_back);
	   DIGITAL_IO_SetOutputLow(&IN4_back);
}

void command_Manager(command decoded_message)
{
	//command decoded_message = decode_Message(message);

	switch(decoded_message.tag)
	{
		case motor_duty:
				set_Motors(decoded_message);
				break;
		case brakes:
				push_Breakes();
				break;
		case line_follower:
				break;
		default: break;


	}
}

void get_message(void)
{
	//uint8_t message[3];
	//UART_Receive(&UART_0, message,sizeof(message));// carcatere receptionat
	//UART_Transmit(&UART_0, message, sizeof(message));
	//command_Manager(message);ss
}

