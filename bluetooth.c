/*
 * bluetooth.c
 *
 *  Created on: May 16, 2018
 *      Author: Ryu
 */
#include "CommonHeader.h"

uint8_t msg_is_ready;
uint8_t start_msg [2];
uint8_t message[5];
command cmd_ToDo = {0};

void Rx_message()
{
	while (msg_is_ready == 0)
		  	{
		  		UART_Receive(&UART_0, start_msg, sizeof(start_msg));
		  		if (start_msg [0] == '<' && start_msg [1] == 39)
		  		{
		  			UART_Receive(&UART_0, message, sizeof(message));
		  		}
		  		if (message [3] == 39 && message [4] == '>')
		  		{
		  			msg_is_ready = 1;
		  		}
		  	}
}

void init_values ()
{
	msg_is_ready = 0;
	start_msg [1] = start_msg[0] = 0;
	uint8_t len_msg = sizeof(message)/sizeof(uint8_t);
	for (int i = 0; i <= len_msg; i++)
	{
		message[i] = 0;
	}
}

command decode_Message(uint8_t* message)
{
	command decoded_message;
	decoded_message.tag = (enum_tags) (*((uint8_t*)message));
	decoded_message.motor_value[front]  = (*((uint8_t*)message+1)) & (~(1<<7));/// direction
	decoded_message.sense = ((*((uint8_t*)message+1)) >> 7) & 1;
	if (decoded_message.motor_value[front] <= 0)
	{
		decoded_message.sense = straight;
	}
	else
	{
		if(decoded_message.sense == left)
		{
			decoded_message.motor_value[front] = ~(decoded_message.motor_value[front] |(1<<7));
		}
	}


	decoded_message.motor_value[back]  = (*((uint8_t*)message+2)) & (~(1<<7));////speed
	decoded_message.direction =  ((*((uint8_t*)message+2)) >> 7) & 1;
	if (decoded_message.motor_value[back] <= 0)
	{
		decoded_message.direction = stop;
	}
	else
	{
		if(decoded_message.direction == backward)
		{
			decoded_message.motor_value[back] = ~(decoded_message.motor_value[back] | (1<<7));
		}
	}

	return decoded_message;
}
