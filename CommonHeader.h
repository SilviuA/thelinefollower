/*
 * CommonHeader.h
 *
 *  Created on: May 16, 2018
 *      Author: Ryu
 */

#ifndef COMMONHEADER_H_
#define COMMONHEADER_H_

#include <DAVE.h>

#define dc 10000
#define HOLD_VALUE 127

typedef enum tags{
	hold = 0,
	motor_duty,
	brakes,
	line_follower
}enum_tags;

typedef enum motors{
	front=0,
	back
}enum_mot_location;

typedef enum direction{
	backward=0,
	forward,
	stop
}enum_direction;

typedef enum sense{
	left = 0,
	right = 1,
	straight
}enum_sense;

typedef struct {
	enum_tags tag;
	uint8_t motor_value[2];
	enum_direction direction;
	enum_sense sense;
}command;

/************* BLUETOOTH *************/

extern uint8_t msg_is_ready;
extern uint8_t start_msg [2];
extern uint8_t message[5];
extern command cmd_ToDo;

void init_values ();
void Rx_message();
command decode_Message(uint8_t* message);

/************* MOTOR *************/

void set_Sense(uint8_t sense_Status);
void set_Direction(uint8_t direction_Status);
void set_Motors(command decoded_message);
void push_Breakes(void);
void command_Manager(command);
void get_message(void);

#endif /* COMMONHEADER_H_ */
