/*
 * main.c
 *
 *  Created on: 2018 May 02 22:39:51
 *  Author: Ryu
 */
#include "CommonHeader.h"


int main(void)
{
  DAVE_STATUS_t status;

  status = DAVE_Init();           /* Initialization of DAVE APPs  */

  if(status != DAVE_STATUS_SUCCESS)
  {
    /* Placeholder for error handler code. The while loop below can be replaced with an user error handler. */
    XMC_DEBUG("DAVE APPs initialization failed\n");

    while(1U)
    {

    }
  }
	 //  DIGITAL_IO_SetOutputHigh (&IN2_front);
	 //   DIGITAL_IO_SetOutputLow(&IN1_front);
	  //  DIGITAL_IO_SetOutputHigh (&IN3_back);
	   // DIGITAL_IO_SetOutputLow(&IN4_back);

	   // Setez frecventa ledului la 50 Hz
	    PWM_SetFreq(&motor_back,50);
	  // PWM_SetDutyCycle (&motor_back,dc);

  while(1U)
  {
	    init_values();
	    Rx_message();
	  	cmd_ToDo = decode_Message(message);

	  	// carcatere receptionat
	  	//UART_Transmit(&UART_0, message, sizeof(message));
		command_Manager(cmd_ToDo);

  }
}

/*

void set_Sense(uint8_t sense_Status)
{
	if (sense_Status == (uint8_t)right)
	{
		DIGITAL_IO_SetOutputHigh (&IN1_front);
		DIGITAL_IO_SetOutputLow(&IN2_front);
	}
	else if(sense_Status == (uint8_t)left)
	{
		DIGITAL_IO_SetOutputHigh (&IN2_front);
	    DIGITAL_IO_SetOutputLow(&IN1_front);
	}
}

void set_Direction(uint8_t direction_Status)
{
	if (direction_Status == (uint8_t)forward)
	{
		DIGITAL_IO_SetOutputHigh (&IN3_back);
		DIGITAL_IO_SetOutputLow(&IN4_back);
	}
	else if(direction_Status == (uint8_t)backward)
	{
		DIGITAL_IO_SetOutputHigh (&IN4_back);
	    DIGITAL_IO_SetOutputLow(&IN3_back);
	}
}

void set_Motors(command decoded_message)
{
	set_Direction(decoded_message.direction);
	set_Sense(decoded_message.sense);
	PWM_SetDutyCycle(&motor_back,10000 - 10000/255*((int)decoded_message.motor_status[back]));
	PWM_SetDutyCycle(&motor_front,10000 - 10000/255*((int)decoded_message.motor_status[front]));

}

// USE THIS WITH CARE FOR PASENGERS
void push_Breakes(void)
{
	   DIGITAL_IO_SetOutputLow(&IN2_front);
	   DIGITAL_IO_SetOutputLow(&IN1_front);
	   DIGITAL_IO_SetOutputLow(&IN3_back);
	   DIGITAL_IO_SetOutputLow(&IN4_back);
}

void command_Manager(uint8_t *message)
{
	command decoded_message = decode_Message(message);
	
	switch(decoded_message.tag)
	{
		case 0:
				set_Motors(decoded_message);
				break;
		case 2:
				push_Breakes();
				break;
		case 1:
				break;
		default: break;

			
	}
}

void get_message(void)
{
	uint8_t message[3];
	UART_Receive(&UART_0, message,sizeof(message));// carcatere receptionat
	UART_Transmit(&UART_0, message, sizeof(message));
	//command_Manager(message);

}
*/
